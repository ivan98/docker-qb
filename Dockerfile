FROM alpine:edge

RUN apk add --no-cache \
  --repository=http://dl-cdn.alpinelinux.org/alpine/edge/testing \
  --repository=http://dl-cdn.alpinelinux.org/alpine/edge/community \
  --repository=http://dl-cdn.alpinelinux.org/alpine/edge/main \
  --repositories-file=/dev/null \
  bind-tools \
  curl \
  mediainfo \
  mktorrent \
  shadow \
  su-exec \
  tzdata \
  qbittorrent-nox

ENV TZ="Asia/Singapore" \
  USERID="1000" \
  GROUPID="1000"

COPY entrypoint.sh /entrypoint.sh

RUN chmod a+x /entrypoint.sh

EXPOSE 61818 61818/udp 8080
WORKDIR /data
VOLUME [ "/data" ]

ENTRYPOINT [ "/entrypoint.sh" ]
CMD [ "/usr/bin/qbittorrent-nox" ]

HEALTHCHECK --interval=30s --timeout=10s --start-period=30s \
  CMD curl --fail http://127.0.0.1:8080/api/v2/app/version || exit 1
