#!/bin/sh

WAN_IP=${WAN_IP:-$(dig +short myip.opendns.com @resolver1.opendns.com)}
echo "WAN IP address is ${WAN_IP}"

if expr "${USERID:-""}" : '[0-9]\+' > /dev/null; then
  usermod -u $USERID -o qbittorrent
fi
if expr "${GROUPID:-""}" : '[0-9]\+' > /dev/null; then
  groupmod -g $GROUPID -o qbittorrent
fi

ALT_WEBUI=${ALT_WEBUI:-false}
if [ "${ALT_WEBUI}" != "true" ]; then
  ALT_WEBUI=false
fi

# /data/watch | Default save location
WATCH_DIR="@Variant(\0\0\0\x1c\0\0\0\x1\0\0\0\x16\0/\0\x64\0\x61\0t\0\x61\0/\0w\0\x61\0t\0\x63\0h\0\0\0\x2\0\0\0\x1)"

QBITTORRENT_HOME=`getent passwd qbittorrent | cut -d: -f6`

echo "Creating folders..."
mkdir -p \
  /data \
  /data/config \
  /data/downloads \
  /data/share \
  /data/temp \
  /data/torrents \
  /data/watch \
  /data/webui \
  ~qbittorrent/.config \
  ~qbittorrent/.local/share/data \
  /var/log/qbittorrent

ln -s /data/config ${QBITTORRENT_HOME}/.config/qBittorrent
ln -s /data/share ${QBITTORRENT_HOME}/.local/share/data/qBittorrent

# https://github.com/qbittorrent/qBittorrent/blob/master/src/base/settingsstorage.cpp
if [ ! -f /data/config/qBittorrent.conf ]; then
  echo "Initializing qBittorrent configuration..."
  cat > /data/config/qBittorrent.conf <<EOL
[General]
ported_to_new_savepath_system=true

[Application]
FileLogger\Enabled=true
FileLogger\Path=/var/log/qbittorrent

[AutoRun]
enabled=true
program=/data/share/myprog.sh

[BitTorrent]
Session\AsyncIOThreadsCount=6
Session\Categories=@Variant(\0\0\0\b\0\0\0\x1\0\0\0\x2\0s\0\0\0\n\0\0\0\"\0/\0\x64\0\x61\0t\0\x61\0/\0\x64\0o\0w\0n\0l\0o\0\x61\0\x64\0s\0/\0s)
Session\DisableAutoTMMByDefault=false

[LegalNotice]
Accepted=true

[Preferences]
Bittorrent\AddTrackers=false
Bittorrent\MaxConnecs=5120
Bittorrent\MaxConnecsPerTorrent=384
Connection\InetAddress=${WAN_IP}
Connection\InterfaceListenIPv6=false
Connection\PortRangeMin=61818
Connection\UseUPnP=false
Downloads\FinishedTorrentExportDir=/data/torrents
Downloads\PreAllocation=true
Downloads\SavePath=/data/downloads
Downloads\ScanDirsV2=${WATCH_DIR}
Downloads\StartInPause=false
Downloads\TempPath=/data/temp
Downloads\TempPathEnabled=true
General\Locale=en
General\UseRandomPort=false
IPFilter\Enabled=true
IPFilter\File=/data/share/ipfilter.dat
Queueing\MaxActiveDownloads=64
Queueing\MaxActiveTorrents=64
Queueing\MaxActiveUploads=64
Queueing\QueueingEnabled=true
WebUI\Address=0.0.0.0
WebUI\AlternativeUIEnabled=${ALT_WEBUI}
WebUI\AuthSubnetWhitelist=192.168.1.0/24
WebUI\AuthSubnetWhitelistEnabled=true
WebUI\Enabled=true
WebUI\HTTPS\Enabled=false
WebUI\LocalHostAuth=false
WebUI\Port=8080
WebUI\RootFolder=/data/webui
WebUI\UseUPnP=false
EOL
fi

echo "Fixing perms..."
chown -R qbittorrent:qbittorrent /data "${QBITTORRENT_HOME}" /var/log/qbittorrent

exec su-exec qbittorrent:qbittorrent "$@"
